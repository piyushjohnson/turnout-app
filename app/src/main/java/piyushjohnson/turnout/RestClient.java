package piyushjohnson.turnout;

import android.content.Context;

import com.google.gson.ExclusionStrategy;
import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    private static Retrofit retrofit;

    private static final String BASE_URL = "http://localhost:5134";
    public static enum ROUTES {LOGIN,AUTHENTICATE};

    private static  final HttpLoggingInterceptor logger = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    private static final Gson gson = new Gson().newBuilder().create();

    private RestClient(){}

    public static synchronized Retrofit initInstance(Context context)
    {
        if(retrofit == null)
        {
            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .readTimeout(100,TimeUnit.MINUTES)
                    .addInterceptor(logger)
                    .addInterceptor(new AuthenticationInterceptor(context))
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(httpClient)
                    .build();
        }
        return retrofit;
    }

    public static <S> S createService(Class<S> serviceClass)
    {
        return retrofit.create(serviceClass);
    }
}
