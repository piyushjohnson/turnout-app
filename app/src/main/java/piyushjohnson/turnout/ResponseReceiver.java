package piyushjohnson.turnout;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.ResultReceiver;

import java.io.Serializable;

public class ResponseReceiver extends ResultReceiver {

    private Receiver mReceiver;

    public ResponseReceiver(Handler handler) {
        super(handler);
    }

    public interface Receiver
    {
        public void onReceiveResult(int resultCode,Bundle resultData);
    }

    public void setReceiver(Receiver receiver)
    {
        if(mReceiver == null)
            mReceiver = receiver;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if(mReceiver != null)
            mReceiver.onReceiveResult(resultCode,resultData);
    }
}
