package piyushjohnson.turnout;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class GeofencingReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
       intent.setClass(context,GeofencingService.class);
       GeofencingService.enqueueWork(context,intent);
    }
}
