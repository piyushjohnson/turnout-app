package piyushjohnson.turnout;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobsService extends IntentService implements Callback<JobsClient.Dataset> {

    public static final String ACTION_GETALL = "ACTION_GETALL";
    public static final String ACTION_GETASSIGNED = "ACTION_GETASSIGNED";
    public static final String ACTION_GETPENDING = "ACTION_GETPENDING";
    public static final String ACTION_GETDONE = "ACTION_GETDONE";
    public static final int MSG_SUCCESS = 0;
    public static final int MSG_FAILURE = 1;
    public static final int MSG_FATAL_ERROR = -1;
    private JobsClient jobsClient = null;
    private ResultReceiver responseReceiver;
    private Bundle dataBundle = new Bundle();

    public JobsService() {
        super("JobsService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null) {
            if(jobsClient == null)
            {
                RestClient.initInstance(getApplicationContext());
                jobsClient = RestClient.createService(JobsClient.class);
            }
            final String action = intent.getAction();
            final Bundle data = intent.getExtras();
            responseReceiver = intent.getParcelableExtra("responseReceiver");
            switch (Objects.requireNonNull(action))
            {
                case ACTION_GETALL:
                    handleActionGetAll(data);
                    break;
                case ACTION_GETASSIGNED:
                    handleActionGetAssigned();
                    break;
                case ACTION_GETPENDING:
                    handleActionGetPending(data);
                    break;
                case ACTION_GETDONE:
                    handleActionGetDone(data);
                    break;
                default:
                    break;
            }
        }
    }

    private void handleActionGetDone(Bundle data) {
        jobsClient.getDoneJobs().enqueue(this);
    }

    private void handleActionGetPending(Bundle data) {
        jobsClient.getPendingJobs().enqueue(this);
    }

    private void handleActionGetAssigned() {
        jobsClient.getAssignedJobs().enqueue(this);
    }

    private void handleActionGetAll(Bundle data) {
        jobsClient.getAllJobs().enqueue(this);
    }

    @Override
    public void onResponse(Call<JobsClient.Dataset> call, Response<JobsClient.Dataset> response) {
        if(response.isSuccessful()) {
            dataBundle.putParcelable("dataset", response.body());
            responseReceiver.send(MSG_SUCCESS,dataBundle);
        }
        else
            responseReceiver.send(MSG_FAILURE,null);
    }

    @Override
    public void onFailure(Call<JobsClient.Dataset> call, Throwable t) {

    }
}
