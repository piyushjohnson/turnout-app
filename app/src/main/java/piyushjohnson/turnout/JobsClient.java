package piyushjohnson.turnout;


import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.POST;

public interface JobsClient
{

    @POST("/api/user/getAll")
    Call<Dataset> getAllJobs();

    @POST("/api/user/jobs/assigned")
    Call<Dataset> getAssignedJobs();


    @POST("/api/user/jobs/pending")
    Call<Dataset> getPendingJobs();


    @POST("/api/user/jobs/done")
    Call<Dataset> getDoneJobs();

    class Dataset implements Parcelable {
        private List<Job> jobs;

        public Dataset(List<Job> jobs) {
            this.jobs = jobs;
        }

        protected Dataset(Parcel in) {
        }

        public static final Creator<Dataset> CREATOR = new Creator<Dataset>() {
            @Override
            public Dataset createFromParcel(Parcel in) {
                return new Dataset(in);
            }

            @Override
            public Dataset[] newArray(int size) {
                return new Dataset[size];
            }
        };

        public List<Job> getJobs() {
            return jobs;
        }

        public void setJobs(List<Job> jobs) {
            this.jobs = jobs;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
        }
    }

    class Job implements Serializable {

        private String title;
        private String type;
        private String inTime;
        private String outTime;
        private boolean status;
        private double latitude;
        private double longitude;

        public Job(String title, String type, String inTime, String outTime, boolean status, double latitude, double longitude) {
            this.title = title;
            this.type = type;
            this.inTime = inTime;
            this.outTime = outTime;
            this.status = status;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getInTime() {
            return inTime;
        }

        public void setInTime(String inTime) {
            this.inTime = inTime;
        }

        public String getOutTime() {
            return outTime;
        }

        public void setOutTime(String outTime) {
            this.outTime = outTime;
        }

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }
    }

}

