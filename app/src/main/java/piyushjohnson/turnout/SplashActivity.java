package piyushjohnson.turnout;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Spinner;
import android.widget.Toast;

public class SplashActivity extends AppCompatActivity implements ResponseReceiver.Receiver {

    Intent userServiceIntent;
    ResponseReceiver responseReceiver;

    private String LOG_TAG = "SplashActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        responseReceiver = new ResponseReceiver(new Handler());
        responseReceiver.setReceiver(this);
        userServiceIntent = new Intent(SplashActivity.this,UserService.class);

        Authenticate();
        Log.d(LOG_TAG,"onCreate() called");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG,"onStart() called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(LOG_TAG,"onPause() called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(LOG_TAG,"onStop() called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG,"onDestroy() called");
    }

    public void Authenticate()
    {
        userServiceIntent.setAction(UserService.ACTION_AUTHENTICATE)
            .putExtra("responseReceiver",responseReceiver);
        startService(userServiceIntent);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        if(resultCode == UserService.MSG_SUCCESS) {
            if (userServiceIntent.getAction().equals(UserService.ACTION_AUTHENTICATE)) {
                startActivity(new Intent(this, HomeActivity.class));
                finish();
                return;
            }
        }
        else if(resultCode == UserService.MSG_FAILURE)
            startActivity(new Intent(this,LoginActivity.class));
        else if(resultCode == UserService.MSG_FATAL_ERROR) {
            Toast.makeText(this, "Server Connection Error", Toast.LENGTH_SHORT).show();
        }
    }
}
