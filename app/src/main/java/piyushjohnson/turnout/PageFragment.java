package piyushjohnson.turnout;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class PageFragment extends Fragment {

    private String pageTitle;
    private TextView textView;

    private List<JobsClient.Job> jobs;
    private JobsClient.Dataset dataset;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private InteractionListener interactionListener;

    public interface InteractionListener {
        void onFragmentClickEvent(View v);
    }

    public enum PAGE_TYPE {
        ASSIGNED,PENDING,DONE
    }

    public PageFragment() {
        Log.d(pageTitle,"PageFragment() called");
    }

    public static PageFragment newInstance(String pageTitle, JobsClient.Dataset dataset) {
        Bundle args = new Bundle();
        args.putString("title",pageTitle);
        args.putParcelable("dataset",dataset);
        PageFragment fragment = new PageFragment();
        fragment.setArguments(args);
        Log.d(pageTitle,"PageFragment.newInstance() called");
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageTitle = getArguments().getString("title");
        dataset = getArguments().getParcelable("dataset");
        Log.d(pageTitle,"PageFragment onCreate() called");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(pageTitle,"PageFragment onStart() called");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof InteractionListener)
        {
         interactionListener = (InteractionListener)context;
        }
        else
        {
            throw new RuntimeException(context.toString() + " must implement Interaction Listener");
        }
        Log.d(pageTitle,"PageFragment onAttach() called");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(pageTitle,"PageFragment onCreateView() called");
        return inflater.inflate(R.layout.fragment_page, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewGroup layout = ((ViewGroup)view);
//        textView = layout.findViewById(R.id.pageTitle);
        recyclerView = layout.findViewById(R.id.recyclerView);


        Log.d(pageTitle,"PageFragment onViewCreated() called");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(dataset != null)
        {
            layoutManager = new LinearLayoutManager(getContext());

            recyclerView.setLayoutManager(layoutManager);
            adapter = new MultiPageAdapter(getContext(), dataset, new MultiPageAdapter.AdapterClickListener() {
                @Override
                public void onClick(View v) {
                    interactionListener.onFragmentClickEvent(v);
                }
            });
            recyclerView.setAdapter(adapter);
        }
        Log.d(pageTitle,"PageFragment onActivityCreated() called");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        interactionListener = null;
        Log.d(pageTitle,"PageFragment onDetach() called");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(pageTitle,"PageFragment onDestroyView() called");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(pageTitle,"PageFragment onStop() called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(pageTitle,"PageFragment onDestroy() called");
    }
}

