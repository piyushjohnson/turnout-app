package piyushjohnson.turnout;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.io.IOException;
import java.util.List;

public class GeofencingService extends JobIntentService {


    List<Address> addresses;
    Location location;
    private static int JOB_ID = 100;

    public GeofencingService() {
        super();
    }

    public static void enqueueWork(Context context,Intent intent)
    {
       enqueueWork(context,GeofencingService.class,JOB_ID,intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if(geofencingEvent.hasError())
        {
            TrackActivity.getLog(geofencingEvent.getErrorCode() + "");
        }
        int transition = geofencingEvent.getGeofenceTransition();
        if(transition == Geofence.GEOFENCE_TRANSITION_ENTER)
        {
            TrackActivity.getLog("Entered geofence");
            location = geofencingEvent.getTriggeringLocation();
            try {
                addresses = new Geocoder(this).getFromLocation(location.getLatitude(),location.getLongitude(),1);
                sendNotification("Tracked Location(Entered)",addresses);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        if(transition == Geofence.GEOFENCE_TRANSITION_EXIT)
        {
            TrackActivity.getLog("Exited geofence");
            location = geofencingEvent.getTriggeringLocation();
            try {
                addresses = new Geocoder(this).getFromLocation(location.getLatitude(),location.getLongitude(),1);
                sendNotification("Tracked Location(Exited)",addresses);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Service","Destroyed");
    }

    public void sendNotification(String title, List<Address> address)
    {

        String CHANNEL_ID = "Turnout";
        CharSequence name = "Turnout App";
        String Description = "Tracked Location Notification";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,CHANNEL_ID);
        builder.setContentTitle(title);
        builder.setContentText(address.get(0).getAddressLine(0));
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setAutoCancel(true);
        builder.setContentIntent(PendingIntent.getActivity(this,1,new Intent(this,TrackActivity.class), PendingIntent.FLAG_UPDATE_CURRENT));
        builder.setPriority(Notification.PRIORITY_DEFAULT);

        NotificationManager notificationManager =(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mChannel.setShowBadge(false);
            notificationManager.createNotificationChannel(mChannel);
        }

        notificationManager.notify("located",100,builder.build());
    }

}
