package piyushjohnson.turnout;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

public class UserFunctions {

    private Intent userServiceIntent;
    private ResponseReceiver responseReceiver;
    private  Context context;
    private UserFunctions userFunctions;

    private UserFunctions(Context context) {
        this.context = context;
        responseReceiver = new ResponseReceiver(new Handler());
        responseReceiver.setReceiver((ResponseReceiver.Receiver) this.context);
        userServiceIntent = new Intent(context,UserService.class);
    }

    public UserFunctions getInstance(Context context)
    {
        if(userFunctions == null)
            return new UserFunctions(context);
        return userFunctions;
    }
}
