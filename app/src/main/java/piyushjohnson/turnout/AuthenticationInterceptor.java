package piyushjohnson.turnout;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Piyush Johnson on 24-06-2018.
 */

public class AuthenticationInterceptor implements Interceptor {

    Context context;
    SharedPreferences preferences;

    public AuthenticationInterceptor(Context context)
    {
            this.context = context;
            preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
    }


    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        Log.d("App",preferences.getString("token","n/a"));
        Log.i("App",request.headers().toString());
        if(request.headers().get("x-access-token") == null)
        {
            if(preferences.contains("token"))
            {
                String token = preferences.getString("token","");
                request = request.newBuilder()
                        .addHeader("x-access-token",token).build();
                Log.d("Authenticator","Adding Token " + token);
            }
        }

        Response response = chain.proceed(request);
        if(response.isSuccessful())
        {
            if(response.header("x-access-token") != null && response.header("x-username") != null)
            {
                SharedPreferences.Editor editor = preferences.edit();
                String token = response.header("x-access-token","");
                String username = response.header("x-username","");
                editor.putString("token",token).putString("username",username).apply();
                Log.d("Authenticator","Saving Token " + token);
            }
        }

        return response;
    }
}
