package piyushjohnson.turnout;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserService extends IntentService implements Callback<String> {

    public static final String ACTION_LOGIN = "ACTION_LOGIN";
    public static final String ACTION_AUTHENTICATE = "ACTION_AUTHENTICATE";
    public static final String ACTION_VERIFY = "ACTION_VERIFY";
    public static final int MSG_SUCCESS = 0;
    public static final int MSG_FAILURE = 1;
    public static final int MSG_FATAL_ERROR = -1;
    private UserClient userClient = null;
    private ResultReceiver responseReceiver;

    public UserService() {
        super("UserService");

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            if(userClient == null)
            {
                RestClient.initInstance(getApplicationContext());
                userClient = RestClient.createService(UserClient.class);
            }
            final String action = intent.getAction();
            final Bundle data = intent.getExtras();
            responseReceiver = intent.getParcelableExtra("responseReceiver");
            switch (Objects.requireNonNull(action))
            {
                case ACTION_LOGIN:
                    handleActionLogin(data);
                    break;
                case ACTION_AUTHENTICATE:
                    handleActionAuthenticate();
                    break;
                case ACTION_VERIFY:
                    handleActionVerify(data);
                    break;
                    default:
                        break;
            }
        }
    }

    public void handleActionLogin(Bundle bundle)
    {
        UserClient.User user = new UserClient.User(bundle.getString("username"),bundle.getString("password"));
        userClient.Login(user).enqueue(this);
    }

    public void handleActionAuthenticate()
    {
        userClient.Authenticate().enqueue(this);
    }

    public void handleActionVerify(Bundle bundle)
    {
        UserClient.User user = new UserClient.User();
        user.setOtp(bundle.getString("otp"));
        userClient.Verify(user).enqueue(this);
        Log.d("SERVICE","verify");
    }

    @Override
    public void onResponse(Call<String> call, Response<String> response) {
        if(response.isSuccessful() && Objects.equals(response.body(),"success"))
            responseReceiver.send(MSG_SUCCESS,null);
        else
           responseReceiver.send(MSG_FAILURE,null);
    }

    @Override
    public void onFailure(Call<String> call, Throwable t) {
        t.printStackTrace();
        responseReceiver.send(MSG_FATAL_ERROR,null);
    }
}
