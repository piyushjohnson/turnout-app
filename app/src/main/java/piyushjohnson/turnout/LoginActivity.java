package piyushjohnson.turnout;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.text.TextUtilsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity implements ResponseReceiver.Receiver, View.OnClickListener {
    AppCompatButton loginButton,authButton;
    BottomSheetDialog verifyDialog;
    Snackbar notifyBar;
    EditText userName,passWord,otp;

    ResponseReceiver responseReceiver;
    Intent userServiceIntent;

    private String LOG_TAG = "LoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginButton = findViewById(R.id.button);
        userName = findViewById(R.id.userName);
        passWord = findViewById(R.id.passWord);

//        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().clear().apply();

        responseReceiver = new ResponseReceiver(new Handler());
        responseReceiver.setReceiver(this);
        userServiceIntent = new Intent(LoginActivity.this,UserService.class);

        prepareVerifyDialog();

        loginButton.setOnClickListener(this);
        if(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).contains("token")) {
            Authenticate();
        }
        Log.d(LOG_TAG,"onCreate() called");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG,"onStart() called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(LOG_TAG,"onResume() called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(LOG_TAG,"onPause() called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(LOG_TAG,"onStop() called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG,"onDestroy() called");
    }

    public void prepareVerifyDialog()
    {
        View verifyDialogLayout = LayoutInflater.from(this).inflate(R.layout.sheet_verify,null);

        verifyDialog = new BottomSheetDialog(this);
        verifyDialog.setContentView(verifyDialogLayout);
        verifyDialog.setCancelable(false);
        verifyDialog.setTitle("Verify Identity");
        otp = verifyDialog.findViewById(R.id.otp);
        authButton = verifyDialog.findViewById(R.id.button2);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.button:
                handleLogin();
                break;
            case R.id.button2:
                verifyDialog.dismiss();
                handleVerify();
                break;
        }
    }

    public void prepareNotifyBar(String message,int color)
    {
        notifyBar =  Snackbar.make(findViewById(R.id.parent_layout),message,Snackbar.LENGTH_LONG);
        notifyBar.getView().setBackgroundColor(getResources().getColor(color));
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        if(resultCode == UserService.MSG_SUCCESS) {
            if(userServiceIntent.getAction().equals(UserService.ACTION_LOGIN)) {
                prepareNotifyBar("Logged in", R.color.colorAccent);
                verifyDialog.show();
                authButton.setOnClickListener(this);
                Authenticate();
                return;
            }
            if(userServiceIntent.getAction().equals(UserService.ACTION_VERIFY) || userServiceIntent.getAction().equals(UserService.ACTION_AUTHENTICATE)) {
                startActivity(new Intent(this, HomeActivity.class));
                finish();
                return;
            }

        }
        else if (resultCode == UserService.MSG_FAILURE)
            prepareNotifyBar("Failed, Try again",R.color.colorError);
        else if(resultCode == UserService.MSG_FATAL_ERROR)
            prepareNotifyBar("Internal Failure",R.color.colorError);
        notifyBar.show();
    }

    public void handleLogin()
    {
        if(validateInputs())
        {
            Bundle dataBundle = new Bundle();
            dataBundle.putString("username",userName.getText().toString());
            dataBundle.putString("password",passWord.getText().toString());
            userServiceIntent.setAction(UserService.ACTION_LOGIN)
                    .putExtras(dataBundle)
                    .putExtra("responseReceiver",responseReceiver);
            startService(userServiceIntent);
        }
    }

    public boolean validateInputs()
    {
        if(TextUtils.isEmpty(userName.getText().toString()) || TextUtils.isEmpty(passWord.getText().toString()))
        {
            userName.setError("Enter valid username");
            passWord.setError("Enter valid password");
            return false;
        }
        return true;
    }

    public void handleVerify()
    {
        Bundle dataBundle = new Bundle();
        dataBundle.putString("otp",otp.getText().toString());
        userServiceIntent.setAction(UserService.ACTION_VERIFY)
                .putExtras(dataBundle)
                .putExtra("responseReceiver",responseReceiver);
        startService(userServiceIntent);
    }

    public void Authenticate()
    {
        userServiceIntent.setAction(UserService.ACTION_AUTHENTICATE)
                .putExtra("responseReceiver",responseReceiver);
        startService(userServiceIntent);
    }
}
