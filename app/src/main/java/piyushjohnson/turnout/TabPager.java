package piyushjohnson.turnout;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class TabPager extends FragmentPagerAdapter {

    private FragmentManager fragmentManager;
    private int TOTAL_PAGES = 3;
    private String PAGE_TITLES[] = {"Assigned","Pending","Done"};
    private JobsClient.Dataset dataset;

    public TabPager(FragmentManager fm,JobsClient.Dataset dataset) {
        super(fm);
        this.fragmentManager = fm;
        this.dataset = dataset;
        Log.d("FRAGMENT","Adapter Adapter() called");
    }

    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    @Override
    public Fragment getItem(int position) {
        Log.d("FRAGMENT","Adapter getItem() called fragment at" + position);
        return PageFragment.newInstance(PAGE_TITLES[position],filterDataset(position));
    }

    private JobsClient.Dataset filterDataset(int position)
    {
        ListIterator<JobsClient.Job> datasetIterator = this.dataset.getJobs().listIterator();
        List<JobsClient.Job> jobList = new ArrayList<>();
        JobsClient.Job job;
        while(datasetIterator.hasNext())
        {
            job = datasetIterator.next();
            if(job.getType().equals(PAGE_TITLES[position].toLowerCase()))
                jobList.add(job);
        }
        return new JobsClient.Dataset(jobList);
    }

    @Override
    public int getCount() {
//        Log.d("FRAGMENT","Adapter getCount() called");
        return TOTAL_PAGES;
    }

    @Override
    public CharSequence getPageTitle(int position) {
//        Log.d("FRAGMENT","Adapter getPageTitle() called for position " + position);
        return PAGE_TITLES[position];
    }

}
