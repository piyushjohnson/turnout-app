package piyushjohnson.turnout;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class TrackActivity extends AppCompatActivity implements OnMapReadyCallback {

    GoogleMap map;
    LocationRequest locationRequest;
    SupportMapFragment mapFragment;
    Geocoder geocoder;
    LatLng latLng,select,current;
    PendingIntent pendingIntent;
    LocationCallback locationCallback;
    FusedLocationProviderClient providerClient;
    GeofencingClient geofencingClient;

    List<Geofence> geofenceList;
    List<Address> addressList;

    TextView titleText,locationText,timeText;
    BottomSheetBehavior bottomSheetBehavior;

    boolean track = false;
    boolean started = false;
    boolean addedGeofences = false;
    boolean granted = false;
    String username;
    private static final int MIN_INTERVAL = 5 * 1000;
    private static final int FASTEST_INTERVAL = 2 * 1000;
    private static final int EXPIRE_INTERVAL = 600 * 1000;

    public static String TRACK = "TRACK";
    private Socket mSocket;

    private String LOG_TAG = "TrackActivity";

    {
        try {
            mSocket = IO.socket("http://localhost:5132/user");
        } catch (URISyntaxException e) { e.printStackTrace(); }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track);

        mSocket.connect();

        username = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("username","guest");

        titleText = (TextView) findViewById(R.id.titleText);
        locationText = (TextView) findViewById(R.id.locationText);
        timeText = (TextView) findViewById(R.id.timeText);
        bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.design_bottom_sheet));

        bottomSheetBehavior.setPeekHeight(BottomSheetBehavior.PEEK_HEIGHT_AUTO);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);;

        if(getIntent() != null) {
            Intent receivedIntent = getIntent();
            if(receivedIntent.getAction().equals(TRACK))
            {
                select = new LatLng(receivedIntent.getDoubleExtra("latitude",0),receivedIntent.getDoubleExtra("longitude",0));
                titleText.setText(receivedIntent.getStringExtra("title"));
                timeText.setText("Reach by " + receivedIntent.getStringExtra("inTime"));
            }
        }

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.maps);
        mapFragment.getMapAsync(this);
        geocoder = new Geocoder(this);
        geofenceList = new ArrayList<>();
        providerClient = getFusedLocationProviderClient(this);
        geofencingClient = LocationServices.getGeofencingClient(this);

        trackMyLocation();
        Log.d(LOG_TAG,"onCreate() called");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG,"onStart() called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(LOG_TAG,"onPause() called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(granted)
        {
            recreate();
        }
        Log.d(LOG_TAG,"onResume() called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(LOG_TAG,"onStop() called");
    }

    @Override
    protected void onDestroy() {
        if (track) {
            providerClient.removeLocationUpdates(locationCallback);
        }
        if(addedGeofences)
        {
            geofencingClient.removeGeofences(pendingIntent);
        }
        if(mSocket.connected())
        {
            mSocket.disconnect();
        }
        Log.d(LOG_TAG,"onDestroy() called");
        super.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        if(!checkPermission())
        {
            granted = true;
           return;
        }

        map = googleMap;
        map.setMyLocationEnabled(true);
        if(select != null)
            onMapInit();
        Log.d(LOG_TAG,"onMapReady() called");
    }

    public void onMapInit() {
        try {
            addressList = geocoder.getFromLocation(select.latitude, select.longitude, 4);
        } catch (IOException e) {
            e.printStackTrace();
        }
        latLng = new LatLng(select.latitude,select.longitude);
        locationText.setText(addressList.get(0).getAddressLine(0));
        map.addMarker(new MarkerOptions().position(latLng).title(addressList.get(0).getAddressLine(0)));
        map.addCircle(new CircleOptions().radius(20).center(latLng).strokeWidth(2.0f));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f));
        if(!started)
        {
            getGeofence(select);
            startGeofencing();
            started = true;
        }
//        map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
//        map.getUiSettings().setAllGesturesEnabled(true);
//        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,16.0f));
        Log.d(LOG_TAG,"onMapInit() called");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1007:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(TrackActivity.this, "granted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(TrackActivity.this, "declined", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void getGeofence(LatLng select) {
        geofenceList.add(new Geofence.Builder()
                .setRequestId("1")
                .setCircularRegion(select.latitude, select.longitude, 120)
                .setExpirationDuration(EXPIRE_INTERVAL)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build()
        );
        Log.d(LOG_TAG, "getGeofence() called [Created geofence]");
    }

    public PendingIntent getGeofencingPendingIntent() {
        Intent intent = new Intent(this, GeofencingReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Log.d(LOG_TAG,"getGeofencingPendingIntent() called [Create geofence pending intent]");
        return pendingIntent;
    }

    public GeofencingRequest getGeofenceRequest() {
        Log.d(LOG_TAG,"getGeofencingRequest() called [Create geofence request]");
        return new GeofencingRequest.Builder()
                .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
                .addGeofences(geofenceList)
                .build();
    }

    public void startGeofencing() {

        if(!checkPermission())
        {
            Log.d(LOG_TAG,"Permission requested");
        }

        geofencingClient.addGeofences(getGeofenceRequest(), getGeofencingPendingIntent())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(LOG_TAG, "startGeofencing() called [Added geofence]");
                        addedGeofences = true;
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                        Log.d(LOG_TAG,"startGeofencing() called [Failed to add geofence]");
                    }
                });
    }

    public void getLastLocation() {

        if(!checkPermission())
        {
            return;
        }

        providerClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            onLocationChanged(location);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(TrackActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void trackMyLocation() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(MIN_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        if(!checkPermission())
        {
            return;
        }

        locationCallback = new LocationCallback() {

            @Override
            public void onLocationResult(LocationResult locationResult) {
                onLocationChanged(locationResult.getLastLocation());
            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                super.onLocationAvailability(locationAvailability);
                /*if(!locationAvailability.isLocationAvailable())
                {
                    LocationSettingsRequest settingsRequest = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest).build();
                    SettingsClient settingsClient = LocationServices.getSettingsClient(TrackActivity.this);
                    settingsClient.checkLocationSettings(settingsRequest)
                            .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                                @Override
                                public void onSuccess(LocationSettingsResponse locationSettingsResponse) {

                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    if (e instanceof ResolvableApiException) {
                                        try {
                                            ResolvableApiException resolvable = (ResolvableApiException) e;
                                            resolvable.startResolutionForResult(TrackActivity.this,0x1);
                                        } catch (IntentSender.SendIntentException sendEx) {
                                            // Ignore the error.
                                        }
                                    }
                                }
                            });
                }*/
            }
        };
        providerClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }

    public void onLocationChanged(Location location) {
        try {
            addressList = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        locationText.append(addressList.get(0).getLocality());
        current = new LatLng(location.getLatitude(), location.getLongitude());
        mSocket.emit("update",username,current.latitude,current.longitude);
//        map.addMarker(new MarkerOptions().position(latLng).title(addressList.get(0).getAddressLine(0)));
//        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 8.0f));
//        textView.setText(location.getLatitude() + " , " + location.getLongitude() + "\n Name : " + address.get(0).getAddressLine(0));
//        Toast.makeText(this, location.getLatitude() + " , " + location.getLongitude() + "\n Name : " + address, Toast.LENGTH_SHORT).show();

    }

    synchronized public boolean checkPermission()
    {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                return false;
            }
        }
        return true;
    }

    public static void getLog(String s) {
        Log.d("Service", s);
    }
}
