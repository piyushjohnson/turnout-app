package piyushjohnson.turnout;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;


public class MultiPageAdapter extends RecyclerView.Adapter implements View.OnClickListener {

    private JobsClient.Dataset mDataset;
    private Context mContext;
    private LayoutInflater layoutInflater;
    private AdapterClickListener adapterClickListener;

    private enum TYPE {
        ASSIGNED,PENDING,DONE
    }

    public interface AdapterClickListener{
        void onClick(View v);
    }

    @Override
    public void onClick(View view) {
        adapterClickListener.onClick(view);
    }

    public MultiPageAdapter(Context context,JobsClient.Dataset dataset,AdapterClickListener adapterClickListener)
    {
        this.mContext = context;
        this.mDataset = dataset;
        this.layoutInflater = LayoutInflater.from(mContext);
        this.adapterClickListener = adapterClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch (viewType)
        {
            case 0:
                view = layoutInflater.inflate(R.layout.item_assigned,parent,false);
                return new AssignedTypeView(view);
            case 1:
                view = layoutInflater.inflate(R.layout.item_pending,parent,false);
                return new PendingTypeView(view);
            case 2:
                view = layoutInflater.inflate(R.layout.item_done,parent,false);
                return new DoneTypeView(view,viewType);
                default:
                    return  null;

        }
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE.valueOf(mDataset.getJobs().get(position).getType().toUpperCase()).ordinal();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        JobsClient.Job job = mDataset.getJobs().get(position);
        int type = TYPE.valueOf(mDataset.getJobs().get(position).getType().toUpperCase()).ordinal();
        if(job != null)
        {
            switch (type)
            {
                case 0:
                    AssignedTypeView assignedTypeView = ((AssignedTypeView) holder);
                    assignedTypeView.titleText.setText(job.getTitle());
                    assignedTypeView.subtitleText.setText(job.getType());
                    assignedTypeView.inTimeText.setText(job.getInTime());
                    assignedTypeView.startButton.setTag(job.getTitle());
                    assignedTypeView.startButton.setOnClickListener(this);
                   break;
                case 1:
                    PendingTypeView pendingTypeView = ((PendingTypeView) holder);
                    pendingTypeView.titleText.setText(job.getTitle());
                    pendingTypeView.subtitleText.setText(job.getType());
                    pendingTypeView.outTimeText.setText(job.getInTime());
                    pendingTypeView.inTimeText.setText(job.getInTime());
                    pendingTypeView.declineButton.setTag(job.getTitle());
                    pendingTypeView.declineButton.setOnClickListener(this);
                    pendingTypeView.acceptButton.setTag(job.getTitle());
                    pendingTypeView.acceptButton.setOnClickListener(this);
                    break;
                case 2:
                    DoneTypeView doneTypeView = ((DoneTypeView) holder);
                    doneTypeView.titleText.setText(job.getTitle());
                    doneTypeView.subtitleText.setText(job.getType());
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.getJobs().size();
    }

    private static class AssignedTypeView extends RecyclerView.ViewHolder {
        public TextView titleText,subtitleText,inTimeText;
        public ImageButton startButton;
        public AssignedTypeView(View view) {
            super(view);
            titleText = view.findViewById(R.id.titleText);
            subtitleText = view.findViewById(R.id.subtitleText);
            inTimeText = view.findViewById(R.id.inTime);
            startButton = view.findViewById(R.id.startButton);
        }
    }

    private static class PendingTypeView extends RecyclerView.ViewHolder {
        public TextView titleText,subtitleText,inTimeText,outTimeText;
        public ImageButton acceptButton,declineButton;
        public PendingTypeView(View view) {
            super(view);
            titleText = view.findViewById(R.id.titleText);
            subtitleText = view.findViewById(R.id.subtitleText);
            inTimeText = view.findViewById(R.id.inTime);
            outTimeText = view.findViewById(R.id.outTime);
            acceptButton = view.findViewById(R.id.acceptButton);
            declineButton = view.findViewById(R.id.declineButton);
        }
    }

    private static class DoneTypeView extends RecyclerView.ViewHolder {
        public TextView titleText,subtitleText;
        public DoneTypeView(View view,int viewType) {
            super(view);
            titleText = view.findViewById(R.id.titleText);
            subtitleText = view.findViewById(R.id.subtitleText);
        }
    }
}
