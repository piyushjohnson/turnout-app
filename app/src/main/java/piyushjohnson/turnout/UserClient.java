package piyushjohnson.turnout;

import com.google.gson.annotations.SerializedName;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserClient {

    @POST("/api/login")
    Call<String> Login(@Body User user);

    @POST("/api/authenticate")
    Call<String> Authenticate();

    @POST("/api/verify")
    Call<String> Verify(@Body User user);

    class User
    {
        @SerializedName("name")
        String name;

        @SerializedName("pass")
        String password;

        @SerializedName("otp")
        String otp;

        public User() {};

        public User(String name,String password)
        {
            this.name = name;
            this.password = password;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }
    }
}