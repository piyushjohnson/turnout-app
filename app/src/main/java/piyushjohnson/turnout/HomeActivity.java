package piyushjohnson.turnout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.location.LocationServices;

import java.util.ListIterator;

public class HomeActivity extends AppCompatActivity implements ResponseReceiver.Receiver,PageFragment.InteractionListener {

    ActionBar actionBar;
    TabLayout tabLayout;
    ViewPager viewPager;
    JobsClient.Dataset dataset;

    ResponseReceiver responseReceiver;
    Intent jobsServiceIntent;

    private String LOG_TAG = "HomeActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        actionBar = getSupportActionBar();

        /*tabLayout = findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Assigned"));
        tabLayout.addTab(tabLayout.newTab().setText("Pending"));
        tabLayout.addTab(tabLayout.newTab().setText("Done"));*/


        responseReceiver = new ResponseReceiver(new Handler());
        responseReceiver.setReceiver(this);

        jobsServiceIntent = new Intent(HomeActivity.this, JobsService.class);
        jobsServiceIntent.putExtra("responseReceiver", responseReceiver);
        jobsServiceIntent.setAction(JobsService.ACTION_GETALL);
        startService(jobsServiceIntent);

        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.tab_views);
        viewPager.setAdapter(null);
        tabLayout.setupWithViewPager(viewPager);
        String username = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("username","");
        Snackbar.make(findViewById(R.id.parent_layout),"Welcome back " + username,Snackbar.LENGTH_LONG).show();
        Log.d(LOG_TAG,"onCreate() called");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG,"onStart() called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(LOG_TAG,"onResume() called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(LOG_TAG,"onPause() called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG,"onDestroy() called");
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        if(resultCode == JobsService.MSG_SUCCESS) {
            dataset = resultData.getParcelable("dataset");
            viewPager.setAdapter(new TabPager(getSupportFragmentManager(),dataset));
        }
    }

    @Override
    public void onFragmentClickEvent(View v) {
        switch (v.getId())
        {
            case R.id.startButton:
                Toast.makeText(this,"start " + v.getTag(),Toast.LENGTH_SHORT).show();
                startTracking(v.getTag().toString());
                break;
        }
    }

    public void startTracking(String jobTitle)
    {
        Intent trackIntent = new Intent(this,TrackActivity.class);
        JobsClient.Job selectedJob = getJobByTitle(jobTitle);
        trackIntent.putExtra("latitude",selectedJob.getLatitude())
                .putExtra("longitude",selectedJob.getLongitude())
                .putExtra("title",selectedJob.getTitle())
                .putExtra("inTime",selectedJob.getInTime())
                .putExtra("outTime",selectedJob.getOutTime())
                .setAction(TrackActivity.TRACK);
        startActivity(trackIntent);
    }

    public JobsClient.Job getJobByTitle(String jobTitle)
    {
        ListIterator<JobsClient.Job> datasetIterator = this.dataset.getJobs().listIterator();
        JobsClient.Job job = null;
        while(datasetIterator.hasNext())
        {
            job = datasetIterator.next();
            if(job.getTitle().equals(jobTitle))
                break;
        }
        return job;
    }
}
